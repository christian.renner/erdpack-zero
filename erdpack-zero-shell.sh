#!/bin/bash

# URL to send the JSON data
URL="https://example.com/endpoint"  # Replace with the actual URL

# Initialize variables
macos_up_to_date=false
firewall_enabled=false
filevault_enabled=false
password_set=false
password_required_immediately=false
automatic_login_disabled=false
two_factor_auth_status="unknown"
standard_user=false
stealth_mode_enabled=false
find_my_mac_enabled=false

# 1. Check if macOS is up to date
updates=$(softwareupdate -l 2>&1)
if [[ "$updates" == *"No new software available"* ]]; then
    macos_up_to_date=true
else
    macos_up_to_date=false
fi

# 2. Check if Firewall is enabled
firewall_status=$(/usr/libexec/ApplicationFirewall/socketfilterfw --getglobalstate)
if [[ "$firewall_status" == *"enabled"* ]]; then
    firewall_enabled=true
else
    firewall_enabled=false
fi

# 3. Check if FileVault is enabled
filevault_status=$(fdesetup status)
if [[ "$filevault_status" == "FileVault is On." ]]; then
    filevault_enabled=true
else
    filevault_enabled=false
fi

# 4. Check if the user has a password set (cannot check password strength)
password_status=$(dscl . -authonly "$USER" "")
if [ $? -eq 0 ]; then
    password_set=true
else
    password_set=false
fi

# 5. Require password immediately after sleep or screen saver
ask_for_password=$(defaults read com.apple.screensaver askForPassword 2>/dev/null)
ask_for_password_delay=$(defaults read com.apple.screensaver askForPasswordDelay 2>/dev/null)

if [ "$ask_for_password" -eq 1 ] && [ "$ask_for_password_delay" -eq 0 ]; then
    password_required_immediately=true
else
    password_required_immediately=false
fi

# 6. Check if Automatic Login is disabled
automatic_login=$(defaults read /Library/Preferences/com.apple.loginwindow autoLoginUser 2>/dev/null)

if [ -z "$automatic_login" ]; then
    automatic_login_disabled=true
else
    automatic_login_disabled=false
fi

# 9. Two-Factor Authentication for Apple ID cannot be checked via bash script
# Setting status to "unknown"
two_factor_auth_status="unknown"

# 10. Check if current user is a standard user
groups=$(id -Gn "$USER")
if [[ "$groups" == *"admin"* ]]; then
    standard_user=false
else
    standard_user=true
fi

# 13. Check if Firewall Stealth Mode is enabled
stealth_mode=$(/usr/libexec/ApplicationFirewall/socketfilterfw --getstealthmode)
if [[ "$stealth_mode" == *"enabled"* ]]; then
    stealth_mode_enabled=true
else
    stealth_mode_enabled=false
fi

# 20. Check if Find My Mac is enabled (may require sudo)
find_my_mac_status=$(defaults read /Library/Preferences/com.apple.FindMyMac.plist FMMEnabled 2>/dev/null)

if [ "$find_my_mac_status" -eq 1 ]; then
    find_my_mac_enabled=true
else
    find_my_mac_enabled=false
fi

# Create JSON object
json=$(cat <<EOF
{
    "macos_up_to_date": $macos_up_to_date,
    "firewall_enabled": $firewall_enabled,
    "filevault_enabled": $filevault_enabled,
    "password_set": $password_set,
    "password_required_immediately": $password_required_immediately,
    "automatic_login_disabled": $automatic_login_disabled,
    "two_factor_auth_status": "$two_factor_auth_status",
    "standard_user": $standard_user,
    "stealth_mode_enabled": $stealth_mode_enabled,
    "find_my_mac_enabled": $find_my_mac_enabled
}
EOF
)

# Send JSON data via REST call
curl -X POST -H "Content-Type: application/json" -d "$json" "$URL"
